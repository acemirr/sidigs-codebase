package base.sidigs.devcode.data.web

import base.sidigs.base.data.web.model.DevApiResponse
import base.sidigs.devcode.data.model.UserItem
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApiService {

    @GET("users")
    suspend fun getUserPerPage(@Query("page") page: String): DevApiResponse<List<UserItem>>
}