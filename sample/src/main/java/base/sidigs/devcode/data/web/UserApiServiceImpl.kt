package base.sidigs.devcode.data.web

import base.sidigs.base.data.web.model.DevApiResponse
import base.sidigs.devcode.data.model.UserItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

class UserApiServiceImpl(private val api:UserApiService) : UserApiService{
    override suspend fun getUserPerPage(page: String) = api.getUserPerPage(page)
}