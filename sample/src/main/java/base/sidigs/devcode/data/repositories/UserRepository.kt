package base.sidigs.devcode.data.repositories

import base.sidigs.base.data.ResultState
import base.sidigs.devcode.data.model.UserEntity
import base.sidigs.devcode.data.model.UserItem

interface UserRepository {

    suspend fun getUsersPerPage(page: String): ResultState<List<UserItem>>

    suspend fun getUsersFromDb(): ResultState<List<UserEntity>>

    suspend fun saveUserToFavorite(user: UserEntity): Boolean

    suspend fun removeUserFromFavorite(user: UserEntity): Boolean
}