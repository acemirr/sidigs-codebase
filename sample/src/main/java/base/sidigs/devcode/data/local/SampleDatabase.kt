package base.sidigs.devcode.data.local

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import base.sidigs.base.utils.core.AppContext
import base.sidigs.devcode.data.model.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class SampleDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {

        private var INSTANCE: SampleDatabase? = null

        fun getAppDatabase(): SampleDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room
                    .databaseBuilder(AppContext.get(), SampleDatabase::class.java, "SampleDatabase.db")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}