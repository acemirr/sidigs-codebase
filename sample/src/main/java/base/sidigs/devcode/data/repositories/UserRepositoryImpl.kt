package base.sidigs.devcode.data.repositories

import base.sidigs.base.data.ResultState
import base.sidigs.base.data.web.model.DevApiResponse
import base.sidigs.base.utils.core.catchAndGetData
import base.sidigs.devcode.data.local.UserDao
import base.sidigs.devcode.data.model.UserEntity
import base.sidigs.devcode.data.model.UserItem
import base.sidigs.devcode.data.web.UserApiService
import kotlinx.coroutines.CompletableDeferred

class UserRepositoryImpl(private val api:UserApiService, private val db:UserDao): UserRepository {
    override suspend fun getUsersPerPage(page: String): ResultState<List<UserItem>> {
        val res = api.getUserPerPage(page).catchAndGetData<DevApiResponse<List<UserItem>>,List<UserItem>>()
        return res
    }

    override suspend fun getUsersFromDb(): ResultState<List<UserEntity>> {
        return ResultState.success(db.getAllFavoriteUser())
    }

    override suspend fun saveUserToFavorite(user: UserEntity): Boolean {
        return CompletableDeferred(db.insert(user)).start()
    }

    override suspend fun removeUserFromFavorite(user: UserEntity): Boolean {
        return CompletableDeferred(db.remove(user)).start()
    }
}