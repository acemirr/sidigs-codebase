package base.sidigs.devcode.domain.usecase

import base.sidigs.base.data.ResultState
import base.sidigs.devcode.data.model.UserEntity
import base.sidigs.devcode.domain.model.User

interface UserUseCase {
    suspend fun getUserFromPage(page: String): ResultState<ArrayList<User>>
    suspend fun getUserFromDb(): ResultState<ArrayList<User>>
    suspend fun saveUserToFavorite(userModel: UserEntity): Boolean
    suspend fun removeUserFromFavorite(userModel: UserEntity): Boolean
}