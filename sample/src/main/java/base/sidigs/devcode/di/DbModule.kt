package base.sidigs.devcode.di

import base.sidigs.devcode.data.local.SampleDatabase
import org.koin.dsl.module

val dbModule = module {
    single { SampleDatabase.getAppDatabase() }
}