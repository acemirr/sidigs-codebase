package base.sidigs.devcode.di

import base.sidigs.devcode.data.local.SampleDatabase
import base.sidigs.devcode.data.repositories.UserRepository
import base.sidigs.devcode.data.repositories.UserRepositoryImpl
import base.sidigs.devcode.domain.usecase.UserInteractor
import base.sidigs.devcode.domain.usecase.UserUseCase
import base.sidigs.devcode.presentation.main.viewmodel.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val userModule = module {
    // Repos
    single<UserRepository> { UserRepositoryImpl(get(), get()) }

    // Database
    single { get<SampleDatabase>().userDao() }

    // UseCase
    single<UserUseCase> { UserInteractor(get()) }

    // ViewModel
    viewModel { MainViewModel(get()) }
}