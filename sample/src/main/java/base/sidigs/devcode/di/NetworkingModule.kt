package base.sidigs.devcode.di

import base.sidigs.base.data.web.core.createOkHttpClient
import base.sidigs.base.data.web.core.createService
import base.sidigs.devcode.BuildConfig
import base.sidigs.devcode.data.web.UserApiService
import base.sidigs.devcode.data.web.UserApiServiceImpl
import org.koin.dsl.module

val networkingModule = module {
    single { createService(UserApiService::class.java, get(), "https://reqres.in/api/") }
    single { UserApiServiceImpl(get()) }
    single {
        createOkHttpClient(
            interceptors = arrayOf(),
            authenticator = null,
            showDebugLog = BuildConfig.DEBUG
        )
    }
}