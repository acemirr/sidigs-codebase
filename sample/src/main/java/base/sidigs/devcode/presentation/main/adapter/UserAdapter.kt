package base.sidigs.devcode.presentation.main.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import base.sidigs.base.presentation.adapter.DevRecyclerViewAdapter
import base.sidigs.base.presentation.adapter.viewholder.DevEndlessItemViewHolder
import base.sidigs.base.presentation.adapter.viewholder.DevItemViewHolder
import base.sidigs.base.utils.core.getDrawable
import base.sidigs.base.utils.extensions.loadImage
import base.sidigs.devcode.R
import base.sidigs.devcode.domain.model.User

class UserAdapter(
    onclickListener: ((data: User?) -> Unit),
    onLongClickListener: ((data: User?) -> Unit)? = null
) : DevRecyclerViewAdapter<User, DevItemViewHolder<User>>(onclickListener, onLongClickListener) {

    override fun getResourceLayout(viewType: Int) =
        if (viewType == CONTENT) R.layout.item_user else R.layout.item_load_more

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DevItemViewHolder<User> =
        when (viewType) {
            CONTENT -> {
                UserHolder(getView(parent, viewType))
            }
            else -> {
                DevEndlessItemViewHolder(
                    getView(parent, viewType),
                    getLoadMoreListener(),
                    isLoading,
                    loadMoreSkip,
                    loadMoreLimit
                )
            }
        }

    override fun getItemViewType(position: Int) = if (listData[position] != null) CONTENT else LOAD_MORE

    inner class UserHolder(itemView: View) : DevItemViewHolder<User>(itemView) {

        override fun bind(data: User?) {
            with(itemView) {
                data?.let { user ->
                    findViewById<ImageView>(R.id.ivUser).loadImage(
                        c = context,
                        imageUrl = user.avatar,
                        placeHolderResourceId = R.drawable.ic_guest,
                        errorDrawable = getDrawable(R.drawable.ic_guest)
                    )


                    findViewById<TextView>(R.id.tvUserName).text = user.name
                    findViewById<TextView>(R.id.tvUserEmail).text = user.email
                }
            }
        }
    }
}