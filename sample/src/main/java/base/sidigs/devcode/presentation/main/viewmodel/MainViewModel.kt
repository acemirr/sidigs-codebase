package base.sidigs.devcode.presentation.main.viewmodel

import androidx.lifecycle.MutableLiveData
import base.sidigs.base.data.ResultState
import base.sidigs.base.presentation.DevViewModel
import base.sidigs.base.utils.core.logDebug
import base.sidigs.devcode.domain.model.User
import base.sidigs.devcode.domain.usecase.UserUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val useCase: UserUseCase) : DevViewModel() {

    var resultApi =
        MutableLiveData<ResultState<ArrayList<User>>>().apply { value = ResultState.default() }
    var resultDb =
        MutableLiveData<ResultState<ArrayList<User>>>().apply { value = ResultState.default() }

    fun getUsersFromPage(page: String) {
        resultApi.value = ResultState.loading()
        scope.launch {
            val response = withContext(Dispatchers.IO) { useCase.getUserFromPage(page) }
            delay(1000)
            when (response) {
                is ResultState.Success -> {
                    logDebug("VM ${response.data}")
                    resultApi.value = if (response.data.isNullOrEmpty()) ResultState.empty()
                    else ResultState.success(response.data)
                }
                is ResultState.Failure -> showError.postValue(response.message)
                else -> resultApi.postValue(response)
            }
        }
    }

    fun loadMoreUser(page: String) = MutableLiveData<ResultState<ArrayList<User>>>().apply {
        value = ResultState.loading()
        scope.launch {
            when (val response = withContext(Dispatchers.IO) { useCase.getUserFromPage(page) }) {
                is ResultState.Success -> {
                    resultApi.value = if (response.data.isNullOrEmpty()) ResultState.empty()
                    else ResultState.success(response.data)
                }
                is ResultState.Failure -> showError.postValue(response.message)
                else -> resultApi.postValue(response)
            }
        }
    }

    fun getUsersFromDb() {
        resultDb.value = ResultState.loading()
        scope.launch {
            when (val response = withContext(Dispatchers.IO) { useCase.getUserFromDb() }) {
                is ResultState.Success -> {
                    resultDb.value = if (response.data.isNullOrEmpty()) ResultState.empty()
                    else ResultState.success(response.data)
                }
                is ResultState.Failure -> showError.postValue(response.message)
                else -> resultDb.postValue(response)
            }
        }
    }

    fun saveUser(user: User?, action: () -> Unit) {
        if (user != null) {
            scope.launch {
                withContext(Dispatchers.IO) { useCase.saveUserToFavorite(user.toUserEntity()) }
                action.invoke()
            }
        }
    }

    fun removeUser(user: User?, action: () -> Unit) {
        if (user != null) {
            scope.launch {
                withContext(Dispatchers.IO) { useCase.removeUserFromFavorite(user.toUserEntity()) }
                action.invoke()
            }
        }
    }
}