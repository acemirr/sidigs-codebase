package base.sidigs.devcode.presentation.main.view

import androidx.recyclerview.widget.LinearLayoutManager
import base.sidigs.base.data.ResultState
import base.sidigs.base.presentation.DevBindingFragment
import base.sidigs.base.utils.core.displayNotification
import base.sidigs.base.utils.extensions.displayDialog
import base.sidigs.base.utils.extensions.toString
import base.sidigs.devcode.R
import base.sidigs.devcode.databinding.FragmentFavoriteUserBinding
import base.sidigs.devcode.domain.model.User
import base.sidigs.devcode.presentation.main.adapter.UserAdapter
import base.sidigs.devcode.presentation.main.viewmodel.MainViewModel
import base.sidigs.devcode.util.Variables
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.util.Date

class FavoriteUserFragment : DevBindingFragment<FragmentFavoriteUserBinding>() {

    private val vm by sharedViewModel<MainViewModel>()
    private lateinit var userAdapter: UserAdapter

    override fun onResume() {
        super.onResume()
        vm.getUsersFromDb()
    }

    override fun initData() {}

    override fun initUI() {
        userAdapter = UserAdapter({ user ->
            displayDialog(requireContext(), "${user?.name}\'s data", "Email : ${user?.email}", Pair("OK", {}))
        }, { user ->
            displayDialog(
                requireContext(),
                "Remove from favorite?",
                "Are you sure removing ${user?.name} from favorite?",
                Pair("Yes", {
                    vm.removeUser(user) {
                        userAdapter.remove(user)
                        displayNotification(
                            requireContext(),
                            Variables.SAMPLE_CHANNEL_ID,
                            "${user?.name} removed",
                            "user successfully removed at " + Date().toString("dd MMM yy hh:mm"),
                            R.mipmap.ic_launcher
                        )
                        if (userAdapter.listData.isEmpty()) {
                            binding.msvFavorite.showEmptyLayout(R.mipmap.ic_launcher, "There's no user to display")
                        }
                    }
                }),
                Pair("No", {})
            )
        })

        binding.rvUserFavorite.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = userAdapter
        }
    }

    override fun initAction() {}

    override fun initObserver() {
        vm.resultDb.observe(this, {
            when (it) {
                is ResultState.Loading -> binding.msvFavorite.showLoadingLayout()

                is ResultState.Empty -> binding.msvFavorite.showEmptyLayout(R.mipmap.ic_launcher, "There's no user to display")

                is ResultState.Failure -> binding.msvFavorite.showErrorLayout(
                    R.mipmap.ic_launcher,
                    "error",
                    it.message,
                    Pair("Try again") {
                        vm.getUsersFromDb()
                    })

                is ResultState.Success -> {
                    binding.msvFavorite.showDefaultLayout()
                    addUserToList(it.data)
                }
                else -> {}
            }
        })
    }

    private fun addUserToList(data: ArrayList<User>) {
        userAdapter.addOrUpdateAll(data)
    }
}