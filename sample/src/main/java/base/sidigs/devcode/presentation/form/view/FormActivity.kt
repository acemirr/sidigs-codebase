package base.sidigs.devcode.presentation.form.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import base.sidigs.devcode.R

class FormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
    }
}