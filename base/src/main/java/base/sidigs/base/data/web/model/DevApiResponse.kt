package base.sidigs.base.data.web.model

import com.google.gson.annotations.SerializedName

/**
 * * Base API Response
 *
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 */

data class DevApiResponse<MODEL> (

    @SerializedName("status")
    var status: Int? = null,

    @SerializedName("success")
    var success: Boolean? = null,

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("data")
    var data: MODEL? = null,

    @SerializedName("exceptions")
    var exceptions: Any? = null
)