package base.sidigs.base.data.web.core

import okhttp3.Authenticator
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

/**
 * Method to create OkHttpClient
 * @param interceptors list of interceptor, define header or action with header here
 * @param authenticator the authenticator, or basic username and basic password
 * @param showDebugLog display Debug Log?
 * @return OkHttpClient to implement at reactive service
 */
fun createOkHttpClient(
    interceptors: Array<Interceptor>,
    authenticator: Authenticator?,
    showDebugLog: Boolean
): OkHttpClient {

    val builder = OkHttpClient.Builder()
        .readTimeout(120, TimeUnit.SECONDS)
        .connectTimeout(120, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)

    interceptors.forEach {
        builder.addInterceptor(it)
    }

    authenticator?.let {
        builder.authenticator(it)
    }

    if (showDebugLog) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(interceptor).build()
    }
    val dispatcher = Dispatcher()
    dispatcher.maxRequests = 30
    builder.dispatcher(dispatcher)

    return builder.build()
}
