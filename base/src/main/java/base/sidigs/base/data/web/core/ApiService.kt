package base.sidigs.base.data.web.core

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Method to create Retrofit Service
 * @param serviceClass the class service to create
 * @param okhttpClient okHttpClient, with defined header
 * @param baseURl Base Url for endpoint
 */
fun <S> createService(serviceClass: Class<S>, okhttpClient: OkHttpClient, baseURl: String): S {

    val gson = GsonBuilder()
        .create()

    val retrofit = Retrofit.Builder()
        .baseUrl(baseURl)
        .client(okhttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    return retrofit.create(serviceClass)
}