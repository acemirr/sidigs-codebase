package base.sidigs.base.di

import com.google.gson.Gson
import org.koin.dsl.module

/**
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 */

val utilityModule = module {
    single { Gson() }
}