package base.sidigs.base.presentation

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment

/**
 * Base class for fragment
 * @sample base.sidigs.devcode.presentation.main.view.AllUserFragment
 * @constructor instance for fragment class
 * @property layoutResource the layout id of the fragment
 * @property currentActivity the activity where the fragment inflated
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 */

abstract class DevFragment : Fragment(), DevView {

    abstract val layoutResource: Int
    protected var currentActivity: DevActivity? = null

    /**
     * onCreate method, here define Fragment has no options menu.
     * If you need options menu, override this method and setHasOptionsMenu(true)
     * @param savedInstanceState instance of state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    /**
     * Method to inflate fragment's view
     * @param inflater Layout Inflater
     * @param container ViewGroup
     * @param savedInstanceState savedInstance
     * @return View that will be used in fragment
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if (layoutResource!=0) inflater.inflate(layoutResource, container, false)
        else super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onViewReady()
    }

    /**
     * Method to set currentActivity's value and call attachListener
     * @param context and here is the attached activity
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DevActivity) {
            currentActivity = context
            currentActivity?.onFragmentAttached(this.javaClass.simpleName)
        }
    }

    /**
     * Method to notify activity that fragment detached
     */
    override fun onDetach() {
        currentActivity?.onFragmentDetached(this.javaClass.simpleName)
        currentActivity = null
        super.onDetach()
    }

    /**
     * Method to request permission to user
     * @param permissions list of permissions to request
     * @param requestCode requestCode for request process, can be used later
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    /**
     * Method to check if application has permission
     * @param permission name of the permission to check
     * @return is the application has the permission?
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String) =
        Build.VERSION.SDK_INT < Build.VERSION_CODES.M || currentActivity?.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED

    /**
     * Method to set activity's toolbar from fragment
     * @param toolbar Toolbar that defined in XML layout, nullable
     * @param title Title for toolbar, nullable
     * @param isChild Display back button it toolbar?
     */
    override fun setupToolbar(
        toolbar: Toolbar?,
        title: String?,
        isChild: Boolean,
        menu: Int?,
        onMenuListener: ((Int) -> Boolean)?
    ) {
        currentActivity?.setupToolbar(toolbar, title, isChild, menu, onMenuListener)
    }

    /**
     * Method to finish activity where the fragment attached
     */
    override fun finishActivity() {
        currentActivity?.finishActivity()
    }

    /**
     * Series of actions when the fragment is ready
     */
    private fun onViewReady() {
        initData()
        initUI()
        initAction()
        initObserver()
    }

    /**
     * Method to init global variable or data from intent
     */
    abstract fun initData()

    /**
     * Method for UI configuration and initialization
     */
    abstract fun initUI()

    /**
     * Method to action to do in fragment
     */
    abstract fun initAction()

    /**
     * Method to initialize observer
     */
    abstract fun initObserver()

    /**
     * Listener or callback when fragment attached or detached
     */
    interface AttachListener {

        /**
         * Action when fragment attached
         */
        fun onFragmentAttached(tag: String)

        /**
         * Action when fragment detached
         */
        fun onFragmentDetached(tag: String)
    }
}