@file:Suppress("unused")

package base.sidigs.base.presentation

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import java.lang.reflect.ParameterizedType
import java.util.Locale

/**
 * Base class for DataBinding activity
 * @sample base.sidigs.devcode.presentation.main.view.MainActivity
 * @constructor instance for activity class
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 * @property layoutResource the layout id for the activity
 * @property menuId the menu id for the toolbar, null by default
 * @property menuListener the listener for the item menu, null by default
 */
abstract class DevBindingActivity<T : ViewDataBinding> : DevActivity() {

    override val layoutResource = 0
    protected lateinit var binding: T

    private var persistentClass: Class<T>? = null

    init {
        this.persistentClass =
            (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<T>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,getLayoutResId())
    }

    protected fun getLayoutResId():Int{
        val resIdName = persistentClass?.simpleName?.dropLast(7)
        var layoutName = ""
        var lastIndex = 0
        if (resIdName != null) {
            for (i in 1..resIdName.lastIndex) {
                if (resIdName[i].isUpperCase()) {
                    layoutName += resIdName.substring(lastIndex, i) + "_"
                    lastIndex = i
                } else if (i == resIdName.lastIndex) {
                    layoutName += resIdName.substring(lastIndex, resIdName.length)
                }
            }
        }
        return resources.getIdentifier(
            layoutName.toLowerCase(Locale.getDefault()), "layout", packageName
        )
    }

}