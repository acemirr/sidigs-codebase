package base.sidigs.base.presentation.adapter.viewholder

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import base.sidigs.base.R
import base.sidigs.base.presentation.adapter.endless.OnLoadMoreListener

/**
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 * @constructor create a new instance for loading item view
 * @param itemView view of inflated item
 * @param loadMoreListener defined in Adapter
 * @param loading defined in Adapter
 * @param loadMoreSkip defined in Adapter
 * @param loadMoreLimit defined in Adapter
 */
class DevEndlessItemViewHolder<T>(
    itemView: View,
    private val loadMoreListener: OnLoadMoreListener?,
    private val loading: Boolean,
    private val loadMoreSkip: Int,
    private val loadMoreLimit: Int
) : DevItemViewHolder<T>(itemView) {

    /**
     * Method to display loading when loading and text error when the api error
     */
    override fun bind(data: T?) {
        val pbLoadMore:ProgressBar = itemView.findViewById(R.id.pbLoadMore)
        val tvLoadMoreError:TextView = itemView.findViewById(R.id.tvLoadMoreError)
        pbLoadMore.visibility = if (loading) View.VISIBLE else View.INVISIBLE
        tvLoadMoreError.visibility = if (!loading) View.VISIBLE else View.INVISIBLE
        tvLoadMoreError.setOnClickListener {
            loadMoreListener?.onLoadMoreRetryButtonClicked(loadMoreSkip, loadMoreLimit)
        }
    }
}