package base.sidigs.base.presentation.adapter.endless

import androidx.recyclerview.widget.RecyclerView

/**
 * interface when action loading more
 *
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 */
interface OnLoadMoreListener {

    fun onLoadMore(skip: Int?, limit: Int?, totalItemsCount: Int?, view: RecyclerView?)
    fun onLoadMoreRetryButtonClicked(skip: Int?, limit: Int?)
}