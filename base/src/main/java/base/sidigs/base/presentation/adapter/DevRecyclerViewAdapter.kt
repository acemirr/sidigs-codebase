package base.sidigs.base.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import base.sidigs.base.presentation.adapter.endless.DevRecyclerViewScrollListener
import base.sidigs.base.presentation.adapter.endless.OnLoadMoreListener
import base.sidigs.base.presentation.adapter.viewholder.DevItemViewHolder

/**
 * Adapter for RecyclerView
 * Before use : define model data and view holder
 *
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 * @constructor instance of recyclerview's adapter
 * @sample base.sidigs.devcode.presentation.main.adapter.UserAdapter
 * @param onClickListener action when item clicked
 * @param onLongClickListener action when item clicked for long, nullable
 * @property isLoading does the recyclerview currently loading the next item?
 * @property listData list of used item in adapter, for filtering purposes please declare another list in initialized adapter
 * @property loadMoreLimit limit for loading item per page
 * @property loadMoreSkip amount of item to be seen before loading the new page
 */
abstract class DevRecyclerViewAdapter<Data, Holder : DevItemViewHolder<Data>>(
    private val onClickListener: ((data: Data?) -> Unit),
    private val onLongClickListener: ((data: Data?) -> Unit)? = null
) : RecyclerView.Adapter<Holder>() {

    val listData = arrayListOf<Data?>()

    /**
     * Method to declare Layout Resource Id for desired ViewType
     * @param viewType ViewType of item
     * @return Layout Resource Id from ViewType
     */
    abstract fun getResourceLayout(viewType: Int): Int

    /**
     * Method to declare View Holder, if you use more than one ViewType, declare desired View Holder based on ViewType
     * in this method
     * @param parent ViewGroup of item
     * @param viewType ViewType of item
     * @return View Holder of desired item
     */
    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder

    /**
     * Method to define how many item displayed in RecyclerView
     * @return size of list item used
     */
    override fun getItemCount() =
        try {
            listData.size
        } catch (E: Exception) {
            0
        }

    /**
     * Method to bind data at position to it's view
     */
    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.initViewAndAction(listData[position], onClickListener, onLongClickListener)
    }

    /**
     * Method to inflate desired view based on viewType
     * @param parent viewGroup of item
     * @param viewType Type of item in list
     * @return View based on viewType
     */
    protected fun getView(parent: ViewGroup, viewType: Int): View =
        LayoutInflater.from(parent.context).inflate(getResourceLayout(viewType), parent, false)

    /**
     * Method to add item to list data
     * @param item single object of data to be added
     */
    fun add(item: Data?) {
        item?.let { listData.add(it) }
        notifyItemInserted(listData.size - 1)
    }

    /**
     * Method to add item to list data at certain position
     * @param item single object of data to be added
     * @param position position where data will be added
     */
    fun addAt(item: Data?, position: Int) {
        item?.let { listData.add(position, it) }
        notifyItemInserted(position)
    }

    /**
     * Method to add list of item to list data
     * @param items collection of items that will be added to list
     */
    fun addAll(items: List<Data>?) {
        items?.let { listData.addAll(it) }
        notifyDataSetChanged()
    }

    /**
     * Method to add data if not exist, and update if it exist
     * @param item item to be added or updated
     */
    fun addOrUpdate(item: Data) {
        val i: Int = listData.indexOf(item)
        if (i >= 0) {
            listData[i] = item
            notifyItemChanged(i)
        } else {
            add(item)
        }
    }

    /**
     * Method to add or update list of items to list data
     * @param items list of items to be added or updated
     */
    fun addOrUpdateAll(items: List<Data>?) {
        items?.forEach { item ->
            val x: Int = listData.indexOf(item)
            if (x >= 0) {
                listData[x] = item
            } else {
                add(item)
            }
        }
        notifyDataSetChanged()
    }

    /**
     * Method to add or update list of items to start of list data
     * @param items list of items that will be added or updated
     */
    fun addOrUpdateToFirst(items: List<Data>?) {
        items?.forEach { item ->
            val x: Int = listData.indexOf(item)
            if (x >= 0) {
                listData[x] = item
            } else {
                addAt(item, 0)
            }
        }
        notifyDataSetChanged()
    }

    /**
     * Method to add a single item to the start of list data
     * @param item item that will be added to start of list
     */
    fun addToFirst(item: Data?) {
        item?.let { listData.add(0, it) }
        notifyDataSetChanged()
    }

    /**
     * Method to add list of item to start of list data
     * @param items list of item to be added to start of list
     */
    fun addToFirst(items: List<Data>?) {
        items?.let { listData.addAll(0, it) }
        notifyDataSetChanged()
    }

    /**
     * Method to remove item at defined position
     * @param position location of item to be deleted
     */
    fun removeAt(position: Int) {
        if (position >= 0 && position < listData.size) {
            listData.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    /**
     * Method to remove defined item from list data
     * @param item item to be deleted
     */
    fun remove(item: Data?) {
        val position: Int = listData.indexOf(item)
        removeAt(position)
    }

    /**
     * Method to remove all item from list data
     */
    fun clear() {
        listData.clear()
        notifyDataSetChanged()
    }

    //    [ENDLESS CONFIGURATION]
    private var loadMoreListener: OnLoadMoreListener? = null

    /**
     * Is recyclerview is loading?
     */
    var isLoading = true
    private var endlessScrollListener: DevRecyclerViewScrollListener? = null
    private var recyclerView: RecyclerView? = null

    /**
     * amount of item to skip
     */
    var loadMoreSkip = 0

    /**
     * Limit of item to load
     */
    var loadMoreLimit = 10

    /**
     * After inherit Activity or Fragment with onLoadMoreListener,
     * <b>call this method first</b> to set listener
     */
    fun setLoadMoreListener(listener: OnLoadMoreListener) {
        loadMoreListener = listener
    }

    /**
     * Method to get loadMoreListener
     */
    fun getLoadMoreListener() = loadMoreListener

    private fun addLoadMoreListener() {
        endlessScrollListener?.let { listener -> recyclerView?.addOnScrollListener(listener) }
    }

    private fun removeLoadMoreListener() {
        endlessScrollListener?.let { listener -> recyclerView?.removeOnScrollListener(listener) }
    }

    /**
     * Show loadMore Layout, call it in observer loadMore
     */
    fun showLoadMoreLoading() {
        isLoading = true
        if (listData[listData.size - 1] == null) {
            notifyItemChanged(listData.size - 1)
        } else {
            listData.add(null)
            notifyDataSetChanged()
        }
    }

    /**
     * Method to hide loading in recyclerview
     */
    fun hideLoadMoreLoading() {
        if (listData[listData.size - 1] == null) {
            listData.removeAt(listData.size - 1)
            notifyItemRemoved(listData.size - 1)
            addLoadMoreListener()
        }
    }

    /**
     * Method to set listener for recyclerView
     * Call this method after assigning loadMoreListener
     * @param rc recyclerView that will be attached with scrollListener
     */
    fun setEndlessScroll(rc: RecyclerView) {
        recyclerView = rc
        endlessScrollListener = object : DevRecyclerViewScrollListener() {
            override val limit = loadMoreLimit

            override var visibleThreshold = loadMoreLimit - 3

            override fun onLoadMore(skip: Int, limit: Int, totalItemsCount: Int, view: RecyclerView) {
                removeLoadMoreListener()
                loadMoreListener?.onLoadMore(skip, limit, totalItemsCount, view)
                loadMoreSkip = skip
                loadMoreLimit = limit
            }
        }
        recyclerView?.layoutManager?.let { endlessScrollListener?.setLayoutManager(it) }
    }

    /**
     * Method to show error textView
     * Define it in error state observer
     */
    fun showLoadMoreError() {
        isLoading = false
        removeLoadMoreListener()
        if (listData[listData.size - 1] == null) {
            notifyItemChanged(listData.size - 1)
        }
    }

    /**
     * Method to finish load more, and remove callback..
     * Define it in empty state observer
     */
    fun finishLoadMore() {
        if (listData[listData.size - 1] == null) {
            listData.remove(null)
            notifyItemRemoved(listData.size - 1)
            removeLoadMoreListener()
        }
    }

    /**
     * Method to reset state and adding loadmore listener
     */
    fun resetEndlessScroll() {
        endlessScrollListener?.resetState()
        addLoadMoreListener()
    }

    /**
     * viewType to differ content and load more
     */
    companion object {

        const val CONTENT = 9001
        const val LOAD_MORE = 9002
    }
}