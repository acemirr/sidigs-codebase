package base.sidigs.base.presentation

import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import base.sidigs.base.utils.core.logDebug

/**
 * Base class for activity
 * @sample base.sidigs.devcode.presentation.main.view.MainActivity
 * @constructor instance for activity class
 * @author Ahmad Amirudin.
 * @since 11-Mar-21.
 * @property layoutResource the layout id for the activity
 * @property menuId the menu id for the toolbar, null by default
 * @property menuListener the listener for the item menu, null by default
 */
abstract class DevActivity : AppCompatActivity(), DevView, DevFragment.AttachListener {

    protected abstract val layoutResource: Int
    private var menuId: Int? = null
    private var menuListener: ((Int) -> Boolean)? = null

    /**
     * This method declare here to set content view from layout
     * @param savedInstanceState saved bundle
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layoutResource!=0)
            setContentView(layoutResource)
    }

    override fun onStart() {
        super.onStart()
        onViewReady()
    }

    /**
     * Series of actions when the fragment is ready
     */
    private fun onViewReady() {
        initData()
        initUI()
        initAction()
        initObserver()
    }

    /**
     * Method to init global variable or data from intent
     */
    abstract fun initData()

    /**
     * Method for UI configuration and initialization
     */
    abstract fun initUI()

    /**
     * Method to action to do in activity
     */
    abstract fun initAction()

    /**
     * Method to initialize observer
     */
    abstract fun initObserver()

    /**
     * Method to set activity's toolbar
     * @param toolbar Toolbar that defined in XML layout, nullable
     * @param title Title for toolbar, nullable
     * @param isChild Display back button it toolbar?
     */
    override fun setupToolbar(
        toolbar: Toolbar?,
        title: String?,
        isChild: Boolean,
        menu: Int?,
        onMenuListener: ((Int) -> Boolean)?
    ) {
        menuId = menu
        menuListener = onMenuListener
        toolbar?.let {
            setSupportActionBar(it)
            supportActionBar?.let { tb ->
                title?.let { title -> tb.title = title }
                tb.setDisplayHomeAsUpEnabled(isChild)
                invalidateOptionsMenu()
            }
        } ?: run {
            supportActionBar?.let {
                it.title = title
                it.setDisplayHomeAsUpEnabled(isChild)
                invalidateOptionsMenu()
            }
        }
        invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuId?.let { menuInflater.inflate(it, menu) }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        menuListener?.invoke(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    /**
     * Method to request permission to user
     * @param permissions list of permissions to request
     * @param requestCode requestCode for request process, can be used later
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    /**
     * Method to check if application has permission
     * @param permission name of the permission to check
     * @return is the application has the permission?
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String) =
        Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED

    /**
     * Method to attach fragment to layout
     * @param viewRes id of Fragment/ FrameLayout
     * @param fragment fragment to attached
     * @param addToBackStack add the fragment to backStack of application
     */
    fun setFragment(viewRes: Int, fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFragmentManager
            .beginTransaction()
            .replace(viewRes, fragment)

        if (addToBackStack) {
            transaction.addToBackStack(null)
        }

        transaction.commit()
    }

    /**
     * Method to finish current activity
     */
    override fun finishActivity() {
        finish()
    }

    /**
     * Method to call action when fragment attached
     * @param tag fragment's name
     */
    override fun onFragmentAttached(tag: String) {
        logDebug("$tag attached")
    }

    /**
     * Method to call action when fragment detached
     * @param tag fragment's name
     */
    override fun onFragmentDetached(tag: String) {
        logDebug("$tag detached")
    }
}