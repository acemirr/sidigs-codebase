package base.sidigs.base.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * View Holder for RecyclerView
 *
 * @Ahmad Amirudin.
 * @since 11-Mar-21.
 * @sample base.sidigs.devcode.presentation.main.adapter.UserAdapter.UserHolder
 * @constructor instance for recyclerview's view holder
 * @param itemView View that inflated per item
 */
abstract class DevItemViewHolder<Model>(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    /**
     * Method to bind model data with desired view from View Holder
     * @param data data model
     */
    abstract fun bind(data: Model?)

    /**
     * Method to set click and long click listener, lastly bind data with view
     * @param data data model
     * @param onClickListener Action when item clicked, nullable
     * @param onLongClickListener Action when item clicked for long, nullable
     */
    fun initViewAndAction(
        data: Model?,
        onClickListener: ((data: Model?) -> Unit)? = null,
        onLongClickListener: ((data: Model?) -> Unit)? = null
    ) {
        itemView.setOnClickListener { onClickListener?.invoke(data) }
        itemView.setOnLongClickListener {
            onLongClickListener?.let {
                it.invoke(data)
                return@setOnLongClickListener true
            } ?: return@setOnLongClickListener false
        }
        bind(data)
    }
}