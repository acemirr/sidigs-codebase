package base.sidigs.base.utils.core

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import timber.log.Timber

@SuppressLint("all")
fun getDeviceId(context: Context): String {
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun logDebug(message:String?){
    Timber.d(message)
}

fun logError(message:String?){
    Timber.e(message)
}