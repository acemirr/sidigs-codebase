package base.sidigs.base.utils.extensions

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.ImageView.ScaleType.CENTER_CROP
import android.widget.ImageView.ScaleType.CENTER_INSIDE
import android.widget.ImageView.ScaleType.FIT_CENTER
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

/**
 * Method to load image to ImageView, please read carefully what param you can use
 * @param c Context where you load image
 *
 * There's 3 valid object you can load to ImageView here, you <b>must</b> use one of these three
 * @param imageDrawable drawable of the image to load
 * @param imageUrl url of the image to load
 * @param imageRes resource id of the image to load
 *
 * You can add placeHolder when loading Image, choose <i>one of these two</i>
 * @param placeHolderDrawable drawable of the placeHolder
 * @param placeHolderResourceId resource id of the placeholder
 *
 * You can add progressBar when loading the image, for that pass value for this parameter
 * @param progressBar progressbar to display when image is loading
 *
 * You can add error image when the process failed, choose <i> one of these two</i>
 * @param errorDrawable drawable of the error image
 * @param errorResourceId resource id of error image
 *
 * You can set the ScaleType of the image, if not defined the default value is CENTER_CROP
 * @param scaleType scaleType of image that will be loaded
 *
 */
fun ImageView.loadImage(
    c: Context,
    imageDrawable: Drawable? = null,
    imageUrl: String? = null,
    imageRes: Int? = null,
    placeHolderDrawable: Drawable? = null,
    placeHolderResourceId: Int? = null,
    progressBar: ProgressBar? = null,
    errorResourceId: Int? = null,
    errorDrawable: Drawable? = null,
    scaleType: ScaleType = CENTER_CROP
) {
    if (!isValidContext(c)) return

    val options = RequestOptions()

    when (scaleType) {
        CENTER_INSIDE -> options.centerInside()
        CENTER_CROP -> options.centerCrop()
        FIT_CENTER -> options.fitCenter()
        else -> options.centerCrop()
    }

    placeHolderDrawable?.let { drawable -> options.placeholder(drawable) }
    placeHolderResourceId?.let { resourceId -> options.placeholder(resourceId) }

    errorDrawable?.let { drawable -> options.error(drawable) }
    errorResourceId?.let { resourceId -> options.error(resourceId) }

    progressBar?.visible()

    val loadImage = Glide.with(c).load(
        when {
            imageDrawable.isNotNull() -> imageDrawable
            imageRes.isNotNull() -> imageRes
            imageUrl.isNotNull() -> imageUrl
            else -> return
        }
    )

    loadImage.apply(options)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean
            ): Boolean {
                progressBar?.gone()
                return false
            }

            override fun onResourceReady(
                resource: Drawable,
                model: Any,
                target: Target<Drawable>,
                dataSource: DataSource,
                isFirstResource: Boolean
            ): Boolean {
                progressBar?.gone()
                return false
            }
        })
        .into(this)
}

/**
 * Method to check if context is valid or not
 * @param context Context that will be used to display image
 * @return is Context valid?
 */
fun isValidContext(context: Context): Boolean {
    val activity = context as? Activity
    return if (activity != null) {
        !(activity.isDestroyed || activity.isFinishing)
    } else {
        true
    }
}